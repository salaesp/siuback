package com.lui.operation;

import org.springframework.beans.factory.annotation.Autowired;
import com.lui.request.CoordinatesRequest;
import com.lui.service.UserService;

/**
 * Created by lui on 10/06/15.
 */
public class SaveCoordinatesOperation extends AbstractLoginAwareOperation<CoordinatesRequest, Boolean> {

	@Autowired
	private UserService userService;

	@Override
	protected Boolean doExecute(CoordinatesRequest request) {
		return userService.saveCoordinates(request);
	}
}
