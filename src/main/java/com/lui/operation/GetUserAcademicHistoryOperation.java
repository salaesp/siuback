package com.lui.operation;

import org.springframework.beans.factory.annotation.Autowired;
import com.lui.guarani.model.AcademicHistory;
import com.lui.request.UserCareerAwareRequest;
import com.lui.service.AcademicHistoryService;

/**
 * Created by lui on 02/06/15.
 */
public class GetUserAcademicHistoryOperation extends AbstractLoginAwareOperation<UserCareerAwareRequest, AcademicHistory> {

	@Autowired
	private AcademicHistoryService academicHistoryService;

	@Override
	protected AcademicHistory doExecute(UserCareerAwareRequest request) {
		return academicHistoryService.getUserAcademicHistory(request);
	}
}
