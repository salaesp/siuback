package com.lui.operation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.lui.guarani.model.CareerInscription;
import com.lui.request.SimpleLoginAwareRequest;
import com.lui.service.CareerService;

/**
 * Created by lui on 31/05/15.
 */
public class GetUserCareerInscriptionsOperation extends AbstractLoginAwareOperation<SimpleLoginAwareRequest, List<CareerInscription>> {

	@Autowired
	private CareerService careerService;


	@Override
	public List<CareerInscription> doExecute(SimpleLoginAwareRequest request) {
		return careerService.listInscriptions(request);
	}
}
