package com.lui.operation;

import java.util.List;

import com.lui.model.NearbyInfo;
import com.lui.model.UserLoginInfo;
import com.lui.request.SimpleLoginAwareRequest;
import org.springframework.beans.factory.annotation.Autowired;
import com.lui.service.UserService;

/**
 * Created by lui on 11/06/15.
 */
public class GetNearbyColleaguesOperation extends AbstractLoginAwareOperation<SimpleLoginAwareRequest,List<NearbyInfo>> {
	@Autowired
	private UserService userService;

	@Override
	protected List<NearbyInfo> doExecute(SimpleLoginAwareRequest request) {
		return userService.getNearbyColleagues(request.getLoginKey());
	}
}
