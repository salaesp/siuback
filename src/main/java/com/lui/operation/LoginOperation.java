package com.lui.operation;

import org.springframework.beans.factory.annotation.Autowired;
import com.lui.controller.request.UserLoginRequest;
import com.lui.response.UserLoginResponse;
import com.lui.service.UserService;

/**
 * Created by lui on 04/06/15.
 */
public class LoginOperation implements Operation<UserLoginRequest, UserLoginResponse> {

	@Autowired
	private UserService userService;

	@Override
	public UserLoginResponse execute(UserLoginRequest request) {
		return userService.login(request.getUsername(), request.getPassword());
	}
}
