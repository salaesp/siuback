package com.lui.operation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.lui.guarani.model.Subject;
import com.lui.request.GetSubjectsRequest;
import com.lui.request.UserCareerAwareRequest;
import com.lui.response.SubjectsResponse;
import com.lui.service.SubjectService;

/**
 * Created by lui on 03/06/15.
 */
public class GetUserSubjectsOperation extends AbstractLoginAwareOperation<GetSubjectsRequest, SubjectsResponse> {
	@Autowired
	private SubjectService subjectService;
	@Autowired
	private Operation<UserCareerAwareRequest, List<Subject>> getAvailableExamsOperation;

	@Override
	protected SubjectsResponse doExecute(GetSubjectsRequest request) {
		SubjectsResponse response = new SubjectsResponse();
		if (request.isIncludeRegularities()) {
			response.setSubjectRegularities(subjectService.listRegularities(request));
		}
		if (request.isIncludeInscriptions()) {
			response.setSubjectInscriptions(subjectService.listInscriptions(request));
		}
		response.setAvailableSubjects(subjectService.listSubjects(request));
		response.setAvailableExams(getAvailableExamsOperation.execute(request));
		return response;
	}
}
