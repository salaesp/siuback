package com.lui.exception;

/**
 * Created by lui on 02/06/15.
 */
public class ErrorMessage {

	public final static String INVALID_TOKEN = "Datos de sesión inválidos";
	public final static String INTERNAL_ERROR = "Error interno";
	public final static String INVALID_ACCESS = "Invalid Access Error";
	public final static String NOT_IN_THE_ZONE = "Se encuentra lejos de la facultad";

	private String message;
	private String cause;

	public ErrorMessage(String message, String cause) {
		this.message = message;
		this.cause = cause;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}
}
