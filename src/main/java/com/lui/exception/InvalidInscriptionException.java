package com.lui.exception;

/**
 * Created by lui on 04/06/15.
 */
public class InvalidInscriptionException extends RuntimeException {

	public InvalidInscriptionException(String message) {
		super(message);
	}
}
