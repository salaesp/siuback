package com.lui.request;

/**
 * Created by lui on 10/06/15.
 */
public class CoordinatesRequest extends SimpleLoginAwareRequest {

    private String coordinates;
    private String status;

    public CoordinatesRequest(String key, String coordinates, String status) {
        super(key);
        this.coordinates = coordinates;
        this.status = status;
    }


    public CoordinatesRequest(String key) {
        super(key);
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
