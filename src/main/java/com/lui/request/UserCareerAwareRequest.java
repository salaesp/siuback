package com.lui.request;

import com.lui.guarani.model.id.CareerPlanID;

/**
 * Created by lui on 03/06/15.
 */
public class UserCareerAwareRequest extends SimpleLoginAwareRequest {

	private CareerPlanID careerPlanID;

	public UserCareerAwareRequest(String key, CareerPlanID careerPlanID) {
		super(key);
		this.careerPlanID = careerPlanID;
	}

	public CareerPlanID getCareerPlanID() {
		return careerPlanID;
	}

	public void setCareerPlanID(CareerPlanID careerPlanID) {
		this.careerPlanID = careerPlanID;
	}
}
