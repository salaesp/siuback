package com.lui.request;

import com.lui.guarani.model.id.SubjectID;

/**
 * Created by lui on 09/06/15.
 */
public class GetInscriptionRequest extends SimpleLoginAwareRequest {
	private SubjectID subjectId;
	private String inscriptionId;

	public GetInscriptionRequest(String key, SubjectID subjectId, String inscriptionId) {
		super(key);
		this.subjectId = subjectId;
		this.inscriptionId = inscriptionId;
	}

	public String getInscriptionId() {
		return inscriptionId;
	}

	public void setInscriptionId(String inscriptionId) {
		this.inscriptionId = inscriptionId;
	}

	public SubjectID getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(SubjectID subjectId) {
		this.subjectId = subjectId;
	}
}
