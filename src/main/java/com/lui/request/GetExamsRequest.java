package com.lui.request;

import com.lui.guarani.model.id.CareerPlanID;

/**
 * Created by lui on 03/06/15.
 */
public class GetExamsRequest extends UserCareerAwareRequest {
	private boolean includeInscriptions;

	public GetExamsRequest(String key, CareerPlanID careerPlanID, boolean includeInscriptions) {
		super(key, careerPlanID);
		this.includeInscriptions = includeInscriptions;
	}

	public boolean isIncludeInscriptions() {
		return includeInscriptions;
	}

	public void setIncludeInscriptions(boolean includeInscriptions) {
		this.includeInscriptions = includeInscriptions;
	}

}
