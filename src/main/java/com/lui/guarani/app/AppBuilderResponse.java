package com.lui.guarani.app;

import java.util.List;

import com.lui.guarani.model.AcademicUnit;
import com.lui.guarani.model.User;

/**
 * Created by lui on 22/05/15.
 */
public class AppBuilderResponse {
	private List<User> userList;
	private List<AcademicUnit> academicUnitList;

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<AcademicUnit> getAcademicUnitList() {
		return academicUnitList;
	}

	public void setAcademicUnitList(List<AcademicUnit> academicUnitList) {
		this.academicUnitList = academicUnitList;
	}
}
