package com.lui.guarani.model;

import java.util.Date;

/**
 * Created by lui on 06/06/15.
 */
public class InscriptionVariant {
	private String id;
	private Date date;

	public InscriptionVariant(String id, Date variantDate) {
		this.id = id;
		this.date = variantDate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
