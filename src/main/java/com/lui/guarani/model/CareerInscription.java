package com.lui.guarani.model;

import java.util.Date;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import com.lui.guarani.model.id.CareerPlanID;

/**
 * Created by lui on 19/05/15.
 */
public class CareerInscription extends AbstractDateIdentifiableObject {

	private CareerPlanID careerPlanID;


	public CareerInscription(String id, String name, Date inscriptionDate, String academicUnit, String career, String careerPlan) {
		super(id, name, inscriptionDate);
		careerPlanID = new CareerPlanID(careerPlan, career, academicUnit);
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public CareerPlanID getCareerPlanID() {
		return careerPlanID;
	}

	public void setCareerPlanID(CareerPlanID careerPlanID) {
		this.careerPlanID = careerPlanID;
	}
}
