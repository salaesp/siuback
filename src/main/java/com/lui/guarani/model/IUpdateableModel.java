package com.lui.guarani.model;

/**
 * Created by lui on 22/05/15.
 */
public interface IUpdateableModel {

	void update();
}
