package com.lui.guarani.model;

import java.util.Date;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import com.lui.guarani.model.id.SubjectID;

/**
 * Created by lui on 19/05/15.
 */
public abstract class AbstractSubjectAwareObject extends AbstractDateIdentifiableObject {
	private SubjectID subject;

	public AbstractSubjectAwareObject(Date date, String id, SubjectID subject, String name) {
		super(id, name, date);
		this.subject = subject;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public SubjectID getSubject() {
		return subject;
	}

	public void setSubject(SubjectID subject) {
		this.subject = subject;
	}
}
