package com.lui.guarani.model.id;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Created by lui on 02/06/15.
 */
public class SubjectID extends CareerPlanID {
	private String subject;

	public SubjectID(CareerPlanID careerPlanID, String subject) {
		super(careerPlanID.getCareerPlan(), careerPlanID.getCareer(), careerPlanID.getAcademicUnit());
		this.subject = subject;
	}

	public SubjectID(String academicUnit, String career, String careerPlan, String subject) {
		super(careerPlan, career, academicUnit);
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}

		SubjectID subjectID = (SubjectID) o;

		if (subject != null ? !subject.equals(subjectID.subject) : subjectID.subject != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (subject != null ? subject.hashCode() : 0);
		return result;
	}
}
