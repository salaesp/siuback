package com.lui.guarani.model.id;

import org.apache.commons.lang3.builder.EqualsBuilder;

/**
 * Created by lui on 02/06/15.
 */
public class CareerPlanID {

	private String careerPlan;
	private String career;
	private String academicUnit;

	public CareerPlanID(String careerPlan, String career, String academicUnit) {
		this.careerPlan = careerPlan;
		this.career = career;
		this.academicUnit = academicUnit;
	}

	public CareerPlanID(SubjectID subjectId) {
		this.career = subjectId.getCareer();
		this.careerPlan = subjectId.getCareerPlan();
		this.academicUnit = subjectId.getAcademicUnit();
	}

	public String getCareerPlan() {
		return careerPlan;
	}

	public void setCareerPlan(String careerPlan) {
		this.careerPlan = careerPlan;
	}

	public String getAcademicUnit() {
		return academicUnit;
	}

	public void setAcademicUnit(String academicUnit) {
		this.academicUnit = academicUnit;
	}

	public String getCareer() {
		return career;
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public boolean isSameCareerPlan(CareerPlanID careerPlanID) {
		EqualsBuilder equalsBuilder = new EqualsBuilder();
		equalsBuilder.append(this.getAcademicUnit(), careerPlanID.getAcademicUnit());
		equalsBuilder.append(this.getCareer(), careerPlanID.getCareer());
		equalsBuilder.append(this.getCareerPlan(), careerPlanID.getCareerPlan());
		return equalsBuilder.isEquals();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		CareerPlanID careerPlanID = (CareerPlanID) o;

		if (academicUnit != null ? !academicUnit.equals(careerPlanID.academicUnit) : careerPlanID.academicUnit != null) {
			return false;
		}
		if (career != null ? !career.equals(careerPlanID.career) : careerPlanID.career != null) {
			return false;
		}
		if (careerPlan != null ? !careerPlan.equals(careerPlanID.careerPlan) : careerPlanID.careerPlan != null) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = careerPlan != null ? careerPlan.hashCode() : 0;
		result = 31 * result + (career != null ? career.hashCode() : 0);
		result = 31 * result + (academicUnit != null ? academicUnit.hashCode() : 0);
		return result;
	}
}
