package com.lui.guarani.model;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

/**
 * Created by lui on 18/05/15.
 */
public class User extends AbstractIdentifiableObject {
	private String password;
	private Student student;

	public User(String id, String username, String password, Student student) {
		super(id, username);
		this.password = password;
		this.student = student;
	}

	public String getUsername() {
		return this.getName();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
}
