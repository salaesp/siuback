package com.lui.dto.model;

import java.util.List;

/**
 * Created by lui on 03/06/15.
 */
public class SubjectsResponseDTO {
	private List<SubjectRegularityDTO> subjectRegularities;
	private List<InscriptionDTO> subjectInscriptions;
	private List<SubjectDTO> availableSubjects;
	private List<SubjectDTO> availableExams;

	public List<SubjectRegularityDTO> getSubjectRegularities() {
		return subjectRegularities;
	}

	public void setSubjectRegularities(List<SubjectRegularityDTO> subjectRegularities) {
		this.subjectRegularities = subjectRegularities;
	}

	public List<InscriptionDTO> getSubjectInscriptions() {
		return subjectInscriptions;
	}

	public void setSubjectInscriptions(List<InscriptionDTO> subjectInscriptions) {
		this.subjectInscriptions = subjectInscriptions;
	}

	public List<SubjectDTO> getAvailableSubjects() {
		return availableSubjects;
	}

	public void setAvailableSubjects(List<SubjectDTO> availableSubjects) {
		this.availableSubjects = availableSubjects;
	}

	public List<SubjectDTO> getAvailableExams() {
		return availableExams;
	}

	public void setAvailableExams(List<SubjectDTO> availableExams) {
		this.availableExams = availableExams;
	}
}
