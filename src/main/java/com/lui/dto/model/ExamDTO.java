package com.lui.dto.model;

/**
 * Created by lui on 02/06/15.
 */
public class ExamDTO extends AbstractDateIdentifiableObjectDTO {
	private SubjectDTO subject;
	private Double grade;


	public Double getGrade() {
		return grade;
	}

	public void setGrade(Double grade) {
		this.grade = grade;
	}

	public SubjectDTO getSubject() {
		return subject;
	}

	public void setSubject(SubjectDTO subject) {
		this.subject = subject;
	}
}
