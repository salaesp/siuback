package com.lui.dto.model;

import java.util.Date;

/**
 * Created by lui on 10/06/15.
 */
public class VisitDTO {
	private String coordinates;
	private Date lastChecked;

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public Date getLastChecked() {
		return lastChecked;
	}

	public void setLastChecked(Date lastChecked) {
		this.lastChecked = lastChecked;
	}
}
