package com.lui.dto.model;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by lui on 02/06/15.
 */
public class AcademicHistoryDTO {
	private List<AcademicHistorySubjectDTO> subjects;

	private Double examPercentage;
	private Double subjectsPercentage;
	private Double fullPercentage;


	public AcademicHistoryDTO() {
		this.subjects = Lists.newArrayList();
	}

	public List<AcademicHistorySubjectDTO> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<AcademicHistorySubjectDTO> subjects) {
		this.subjects = subjects;
	}

	public Double getExamPercentage() {
		return examPercentage;
	}

	public void setExamPercentage(Double examPercentage) {
		this.examPercentage = examPercentage;
	}

	public Double getSubjectsPercentage() {
		return subjectsPercentage;
	}

	public void setSubjectsPercentage(Double subjectsPercentage) {
		this.subjectsPercentage = subjectsPercentage;
	}

	public Double getFullPercentage() {
		return fullPercentage;
	}

	public void setFullPercentage(Double fullPercentage) {
		this.fullPercentage = fullPercentage;
	}
}
