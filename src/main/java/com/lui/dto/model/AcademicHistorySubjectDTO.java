package com.lui.dto.model;

/**
 * Created by lui on 19/08/15.
 */
public class AcademicHistorySubjectDTO {
    private ExamDTO exam;
    private SubjectRegularityDTO subjectRegularity;

    public ExamDTO getExam() {
        return exam;
    }

    public void setExam(ExamDTO exam) {
        this.exam = exam;
    }

    public SubjectRegularityDTO getSubjectRegularity() {
        return subjectRegularity;
    }

    public void setSubjectRegularity(SubjectRegularityDTO subjectRegularity) {
        this.subjectRegularity = subjectRegularity;
    }
}
