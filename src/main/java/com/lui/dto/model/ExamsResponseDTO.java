package com.lui.dto.model;

import java.util.List;

/**
 * Created by lui on 03/06/15.
 */
public class ExamsResponseDTO {


	private List<ExamDTO> exams;
	private List<InscriptionDTO> examInscriptions;

	public List<ExamDTO> getExams() {
		return exams;
	}

	public void setExams(List<ExamDTO> exams) {
		this.exams = exams;
	}

	public List<InscriptionDTO> getExamInscriptions() {
		return examInscriptions;
	}

	public void setExamInscriptions(List<InscriptionDTO> examInscriptions) {
		this.examInscriptions = examInscriptions;
	}
}
