package com.lui.dto.model;

import java.util.Date;

/**
 * Created by lui on 31/05/15.
 */
public class AbstractDateIdentifiableObjectDTO extends AbstractIdentifiableObjectDTO {

	private Date date;

	public Date getDate() {
		return date;
	}

	public void setDate(Date inscriptionDate) {
		this.date = inscriptionDate;
	}
}
