package com.lui.dto.model;

/**
 * Created by lui on 31/05/15.
 */
public class CareerInscriptionDTO extends AbstractDateIdentifiableObjectDTO {
	private AcademicUnitDTO academicUnit;
	private CareerDTO career;
	private CareerPlanDTO careerPlan;

	public AcademicUnitDTO getAcademicUnit() {
		return academicUnit;
	}

	public void setAcademicUnit(AcademicUnitDTO academicUnit) {
		this.academicUnit = academicUnit;
	}

	public CareerDTO getCareer() {
		return career;
	}

	public void setCareer(CareerDTO career) {
		this.career = career;
	}

	public CareerPlanDTO getCareerPlan() {
		return careerPlan;
	}

	public void setCareerPlan(CareerPlanDTO careerPlan) {
		this.careerPlan = careerPlan;
	}
}
