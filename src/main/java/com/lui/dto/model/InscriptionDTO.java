package com.lui.dto.model;

/**
 * Created by lui on 02/06/15.
 */
public class InscriptionDTO extends AbstractSubjectAwareObjectDTO {
	private InscriptionVariantDTO variant;

	public InscriptionVariantDTO getVariant() {
		return variant;
	}

	public void setVariant(InscriptionVariantDTO variant) {
		this.variant = variant;
	}
}
