package com.lui.dto.transformer;

import com.lui.dto.model.AcademicUnitDTO;
import com.lui.guarani.model.AcademicUnit;

/**
 * Created by lui on 02/06/15.
 */
public class AcademicUnitDTOTransformer extends AbstractIdentifiableTransformerDTOTransformer<AcademicUnit, AcademicUnitDTO> {
	@Override
	protected void transform(AcademicUnit input, AcademicUnitDTO output) {

	}

	@Override
	protected AcademicUnitDTO getInstance() {
		return new AcademicUnitDTO();
	}
}
