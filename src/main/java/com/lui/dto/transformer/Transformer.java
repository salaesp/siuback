package com.lui.dto.transformer;

import java.util.List;

/**
 * Created by lui on 31/05/15.
 */
public interface Transformer<I, O> {

	O transform(I in);

	List<O> transform(List<I> in);

}
