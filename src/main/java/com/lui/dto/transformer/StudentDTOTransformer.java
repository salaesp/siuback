package com.lui.dto.transformer;

import com.lui.dto.model.StudentDTO;
import com.lui.guarani.model.Student;

/**
 * Created by lui on 14/06/15.
 */
public class StudentDTOTransformer extends AbstractIdentifiableTransformerDTOTransformer<Student, StudentDTO> {
	@Override
	protected void transform(Student input, StudentDTO output) {

	}

	@Override
	protected StudentDTO getInstance() {
		return new StudentDTO();
	}
}
