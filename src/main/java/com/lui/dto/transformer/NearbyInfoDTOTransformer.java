package com.lui.dto.transformer;

import com.lui.dto.model.CoordinateDTO;
import com.lui.dto.model.NearbyInfoDTO;
import com.lui.dto.model.StudentDTO;
import com.lui.guarani.model.Student;
import com.lui.model.Coordinate;
import com.lui.model.NearbyInfo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by lui on 20/08/15.
 */
public class NearbyInfoDTOTransformer extends AbstractTransformer<NearbyInfo, NearbyInfoDTO>{
    @Autowired
    private Transformer<Student, StudentDTO> studentDTOTransformer;

    @Override
    public NearbyInfoDTO transform(NearbyInfo in) {
        NearbyInfoDTO out = new NearbyInfoDTO();
        out.setStudent(studentDTOTransformer.transform(in.getStudent()));
        Coordinate coordinates = in.getVisit().getCoordinates();
        out.setStatus(in.getVisit().getStatus());
        out.setCoordinate(new CoordinateDTO(coordinates.getLatitude(), coordinates.getLongitude()));
        return out;
    }
}
