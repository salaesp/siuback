package com.lui.dto.transformer;

import com.lui.dto.model.AbstractIdentifiableObjectDTO;
import com.lui.guarani.model.AbstractIdentifiableObject;

/**
 * Created by lui on 02/06/15.
 */
public abstract class AbstractIdentifiableTransformerDTOTransformer<I extends AbstractIdentifiableObject, O extends AbstractIdentifiableObjectDTO> extends AbstractTransformer<I, O> {

	@Override
	public O transform(I in) {
		O out = getInstance();
		out.setId(in.getId());
		out.setName(in.getName());
		this.transform(in, out);
		return out;
	}

	protected abstract void transform(I input, O output);


	protected abstract O getInstance();
}
