package com.lui.dto.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import com.lui.dto.model.SubjectDTO;
import com.lui.dto.model.SubjectRegularityDTO;
import com.lui.guarani.model.Subject;
import com.lui.guarani.model.SubjectRegularity;
import com.lui.service.SubjectService;

/**
 * Created by lui on 02/06/15.
 */
public class SubjectRegularityDTOTransformer extends AbstractDateIdentifiableObjectDTOTransformer<SubjectRegularity, SubjectRegularityDTO> {

	@Autowired
	private SubjectService subjectService;

	@Autowired
	private Transformer<Subject, SubjectDTO> subjectDTOTransformer;

	@Override
	protected void transform(SubjectRegularity input, SubjectRegularityDTO output) {
		Subject subject = subjectService.getById(input.getSubject());
		output.setSubject(subjectDTOTransformer.transform(subject));
	}

	@Override
	protected SubjectRegularityDTO getInstance() {
		return new SubjectRegularityDTO();
	}
}
