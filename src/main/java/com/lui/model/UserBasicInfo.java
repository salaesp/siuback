package com.lui.model;

import java.util.List;

import com.lui.guarani.model.CareerInscription;

/**
 * Created by lui on 10/06/15.
 */
public class UserBasicInfo {
	private Visit lastSeen;
	private List<CareerInscription> careerInscriptions;

	public Visit getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(Visit lastSeen) {
		this.lastSeen = lastSeen;
	}

	public List<CareerInscription> getCareerInscriptions() {
		return careerInscriptions;
	}

	public void setCareerInscriptions(List<CareerInscription> careerInscriptions) {
		this.careerInscriptions = careerInscriptions;
	}
}
