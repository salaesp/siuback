package com.lui.model;

/**
 * Created by lui on 10/06/15.
 */
public class Coordinate {

	private Double longitude;
	private Double latitude;

	public Coordinate(Double latitude, Double longitude) {
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
}
