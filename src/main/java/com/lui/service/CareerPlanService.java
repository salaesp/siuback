package com.lui.service;

import com.lui.guarani.model.CareerPlan;
import com.lui.guarani.model.id.CareerPlanID;

/**
 * Created by lui on 31/05/15.
 */
public interface CareerPlanService {

	CareerPlan getById(CareerPlanID careerPlanID);
}
