package com.lui.service;

import java.util.List;

import com.lui.guarani.model.Student;
import com.lui.model.NearbyInfo;
import com.lui.model.UserLoginInfo;
import com.lui.request.CoordinatesRequest;
import com.lui.response.UserLoginResponse;

/**
 * Created by lui on 14/05/15.
 */
public interface UserService {
	UserLoginResponse login(String username, String password);

	boolean isUserLogged(String key);

	UserLoginInfo getLoggedUser(String key);

	Boolean saveCoordinates(CoordinatesRequest request);

	List<NearbyInfo> getNearbyColleagues(String key);
}
