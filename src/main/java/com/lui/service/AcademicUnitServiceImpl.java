package com.lui.service;

import org.springframework.beans.factory.annotation.Autowired;
import com.lui.facade.GuaraniFacadeImpl;
import com.lui.guarani.model.AcademicUnit;

/**
 * Created by lui on 31/05/15.
 */
public class AcademicUnitServiceImpl implements AcademicUnitService {

	@Autowired
	private GuaraniFacadeImpl guaraniFacade;

	public AcademicUnit getById(String id) {
		return guaraniFacade.getAcademicUnit(id);
	}
}
