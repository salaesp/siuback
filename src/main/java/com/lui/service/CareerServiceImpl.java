package com.lui.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import com.lui.facade.GuaraniFacade;
import com.lui.guarani.model.Career;
import com.lui.guarani.model.CareerInscription;
import com.lui.request.SimpleLoginAwareRequest;

/**
 * Created by lui on 31/05/15.
 */
public class CareerServiceImpl implements CareerService {

	@Autowired
	private GuaraniFacade guaraniFacade;

	public Career getById(String academicUnitId, String careerId) {
		return guaraniFacade.getCareer(academicUnitId, careerId);
	}

	@Override
	public List<CareerInscription> listInscriptions(SimpleLoginAwareRequest request) {
		return guaraniFacade.getCareerInscriptionsForUser(request);
	}
}
