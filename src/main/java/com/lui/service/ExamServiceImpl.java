package com.lui.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.lui.facade.GuaraniFacade;
import com.lui.guarani.model.Exam;
import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.id.SubjectID;
import com.lui.request.AddInscriptionRequest;
import com.lui.request.GetInscriptionRequest;
import com.lui.request.UserCareerAwareRequest;

/**
 * Created by lui on 02/06/15.
 */
public class ExamServiceImpl implements ExamService {

	@Autowired
	private GuaraniFacade guaraniFacade;

	@Override
	public List<Inscription> listInscriptions(UserCareerAwareRequest request) {
		List<Inscription> examInscriptions = guaraniFacade.getExamInscriptions(request);
		final Set<SubjectID> subjectIDs = FluentIterable.from(this.list(request)).transform(new Function<Exam, SubjectID>() {
			@Override
			public SubjectID apply(Exam exam) {
				return exam.getSubject();
			}
		}).toSet();
		return FluentIterable.from(examInscriptions).filter(new Predicate<Inscription>() {
			@Override
			public boolean apply(Inscription examInscription) {
				return !subjectIDs.contains(examInscription.getSubject());
			}
		}).toList();
	}

	@Override
	public Inscription addInscription(AddInscriptionRequest request) {
		return guaraniFacade.addExamInscription(request);
	}

	@Override
	public Inscription getInscription(GetInscriptionRequest request) {
		return guaraniFacade.getExamInscription(request);
	}

	@Override
	public Inscription deleteInscription(GetInscriptionRequest request) {
		return guaraniFacade.deleteExamInscription(request);
	}

	@Override
	public List<Exam> list(UserCareerAwareRequest request) {
		return guaraniFacade.getExams(request);
	}
}
