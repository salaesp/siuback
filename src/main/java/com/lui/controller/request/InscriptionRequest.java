package com.lui.controller.request;

import com.lui.utils.InscriptionAction;

/**
 * Created by lui on 04/06/15.
 */
public class InscriptionRequest {
	private InscriptionAction inscriptionAction;
	private String variant;

	public InscriptionAction getInscriptionAction() {
		return inscriptionAction;
	}

	public void setInscriptionAction(InscriptionAction inscriptionAction) {
		this.inscriptionAction = inscriptionAction;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}
}
