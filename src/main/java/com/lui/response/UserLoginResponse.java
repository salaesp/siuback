package com.lui.response;

import com.lui.utils.ResponseStatus;

/**
 * Created by lui on 14/05/15.
 */
public class UserLoginResponse {
	private String loginKey;
	private ResponseStatus status;

	public UserLoginResponse(String loginKey, ResponseStatus status) {
		this.loginKey = loginKey;
		this.status = status;
	}

	public String getLoginKey() {
		return loginKey;
	}

	public void setLoginKey(String loginKey) {
		this.loginKey = loginKey;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}
}
