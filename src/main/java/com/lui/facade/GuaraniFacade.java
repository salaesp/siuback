package com.lui.facade;

import java.util.List;

import com.lui.guarani.model.AcademicUnit;
import com.lui.guarani.model.Career;
import com.lui.guarani.model.CareerInscription;
import com.lui.guarani.model.CareerPlan;
import com.lui.guarani.model.Exam;
import com.lui.guarani.model.Inscription;
import com.lui.guarani.model.Student;
import com.lui.guarani.model.Subject;
import com.lui.guarani.model.SubjectRegularity;
import com.lui.guarani.model.id.CareerPlanID;
import com.lui.guarani.model.id.SubjectID;
import com.lui.request.AddInscriptionRequest;
import com.lui.request.GetInscriptionRequest;
import com.lui.request.SimpleLoginAwareRequest;
import com.lui.request.UserCareerAwareRequest;

/**
 * Created by lui on 31/05/15.
 */
public interface GuaraniFacade {

	AcademicUnit getAcademicUnit(String id);

	Career getCareer(String academicUnitId, String careerId);

	CareerPlan getCareerPlan(CareerPlanID careerPlanID);

	Subject getSubject(SubjectID subjectID);

	List<CareerInscription> getCareerInscriptionsForUser(SimpleLoginAwareRequest request);

	List<Inscription> getSubjectInscriptions(UserCareerAwareRequest request);

	List<Inscription> getExamInscriptions(UserCareerAwareRequest request);

	List<Exam> getExams(UserCareerAwareRequest request);

	List<SubjectRegularity> getSubjectRegularities(UserCareerAwareRequest request);

	Inscription addSubjectInscription(AddInscriptionRequest request);

	Inscription addExamInscription(AddInscriptionRequest request);

	Inscription getExamInscription(GetInscriptionRequest request);

	Inscription getSubjectInscription(GetInscriptionRequest request);

	Inscription deleteExamInscription(GetInscriptionRequest request);

	Inscription deleteSubjectInscription(GetInscriptionRequest request);

	List<Student> getStudentsFor(SubjectID subject);

}
