package com.lui.facade;

import com.lui.guarani.model.Student;
import com.lui.model.Coordinate;
import com.lui.model.UserLoginInfo;
import com.lui.response.UserLoginResponse;

/**
 * Created by lui on 14/05/15.
 */
public interface UserFacade {
	UserLoginResponse login(String username, String password);

	boolean isUserLoggedByKey(String key);

	UserLoginInfo getLoggedUser(String key);

	void saveCoordinates(String key, Coordinate coordinates, String status);

	UserLoginInfo getUserFor(Student student);

	Student getStudentWithUserId(String userId);

}
