package com.lui.utils;

/**
 * Created by lui on 04/06/15.
 */
public enum InscriptionAction {
	EXAM_INSCRIPTION, SUBJECT_INSCRIPTION;
}
