package com.lui.debug;

/**
 * Created by lui on 16/08/15.
 */
public class DebugExam {
    private String code;
    private Double grade;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }
}
