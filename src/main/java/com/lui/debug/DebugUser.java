package com.lui.debug;

/**
 * Created by lui on 16/08/15.
 */
public class DebugUser {
    private String password;
    private String name;
    private String id;
    private DebugStudent student;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DebugStudent getStudent() {
        return student;
    }

    public void setStudent(DebugStudent student) {
        this.student = student;
    }
}
