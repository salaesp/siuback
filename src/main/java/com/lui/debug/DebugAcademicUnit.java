package com.lui.debug;

import java.util.List;

/**
 * Created by lui on 11/08/15.
 */
public class DebugAcademicUnit {

    private List<DebugCareer> careers;
    private String code;
    private String name;

    public List<DebugCareer> getCareers() {
        return careers;
    }

    public void setCareers(List<DebugCareer> careers) {
        this.careers = careers;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
